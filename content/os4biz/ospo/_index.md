---
title: Open Source Program Offices
seo_title: Open Source Program Offices | Eclipse Foundation
keywords: ['OSPO', 'Open Source Program Offices', 'OSPO.Zone', 'OSPO Alliance']
hide_sidebar: true
hide_page_title: true
container: container-fluid
---

{{< pages/os4biz/ospo >}}

Open Source Program Offices (OSPO) promote the structured and professional
management of open source by companies and administrations.

The Eclipse Foundation, along with a coalition of leading European open source
non-profit organisations, created the [OSPO Alliance](https://ospo-alliance.org),
an open community that brings and shares guidance to organisations willing to
professionally manage the usage, contribution to, and publication of open
source software.

The OSPO Alliance offers a comprehensive set of resources for corporations,
public institutions, and research and academic organisations, including:

- **The Good Governance Initiative** (GGI): a methodological framework to assess
  and improve open-source trust, awareness and strategy within organisations.
- The **OSPO OnRamp** meeting series: an open, neutral, and friendly forum, a
  low-threshold entry point to exchange and learn about the basics of how to
  set up an Open Source Program Office and get started with open source.

The Eclipse Foundation is a leading contributor to the Alliance, and actively
participates in its various task forces. Additionally, we provide consulting
services around OSPOs. Please [contact us](/org/foundation/contact.php) if you
need our experts to help in implementing your winning open source strategy
based on the material from the OSPO Alliance.

Learn more about the [OSPO Alliance](https://ospo-alliance.org) and how it
helps OSPOs around the world to connect.

{{</ pages/os4biz/ospo >}}
