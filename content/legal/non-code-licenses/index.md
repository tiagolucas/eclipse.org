---
title: "Approved Licenses for Non-Code, Example, and Other Content"
author: Mike Milinkovich
keywords:
    - license
    - content
related:
  links:
    - text: Legal resources
      url: /legal
    - text: Guide to legal documents
      url: /projects/handbook#legaldoc
---

December 6, 2017

The following licenses have been approved by the Board of Directors for use with Non-Code Content, as that term is defined in the [Eclipse.org Terms of Use](/legal/terms-of-use/).

1. the [Eclipse Public License 2.0](/legal/epl-2.0/),
2. the [Creative Commons Attribution-Share Alike 3.0 (Unported) license](http://creativecommons.org/licenses/by-sa/3.0/),
3. the [Creative Commons Attribution 3.0 (Unported) License](http://creativecommons.org/licenses/by/3.0/),
4. the [Creative Commons Attribution-Share Alike 4.0 (International) License](http://creativecommons.org/licenses/by-sa/4.0/), or
5. the [Creative Commons Attribution 4.0 (International) License](http://creativecommons.org/licenses/by/4.0/).

In addition, the following licenses have been approved by the Board of Directors for use with example code or build scripts and artifacts within Eclipse projects.

1. the [Eclipse Public License 2.0](/legal/epl-2.0/),
2. the [Eclipse Distribution License](/org/documents/edl-v10.php) (EDL).

For additional details on the definition of example code or build scripts and artifacts and how to seek PMC approval for using the EDL, please refer to the [policy document](/org/documents/Licensing_Example_Code.pdf).
