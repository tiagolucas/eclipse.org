---
title: About File Templates
author: Wayne Beaton
keywords:
  - about
  - legal
  - foundation
page_css_file: public/css/legal/about-file-templates.css
related:
  links:
    - text: EPL-2.0 - `about.html` in plain HTML
      url: epl-2-about.html
    - text: EPL-2.0 - Third-Party Content `about.html` in plain HTML
      url: epl-2-longabout.html
    - text: EPL-2.0 - Legal Documentation for Eclipse Platform Plug-ins and Fragments under the EPL-2.0
      url: /projects/handbook/#legaldoc-plugins
    - text: EPL-1.0 - `about.html` in plain HTML
      url: epl-1-about.html
    - text: EPL-1.0 - Third-Party Content `about.html` in plain HTML
      url: epl-1-longabout.html
    - text: EPL-1.0 - Legal Documentation for Eclipse Platform Plug-ins and Fragments under the EPL-2.0
      url: /projects/handbook/#legaldoc-plugins
---

{{< pages/legal/about-file-templates >}}
