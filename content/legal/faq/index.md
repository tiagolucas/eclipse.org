---
title: "Eclipse Foundation Legal Frequently Asked Questions (FAQ)"
author: Mike Milinkovich
layout: single
hide_page_title: true
keywords:
    - legal
    - faq
    - foundation
---

{{< pages/legal/inject-content >}}
