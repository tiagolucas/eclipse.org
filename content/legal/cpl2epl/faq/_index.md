---
title: CPL To EPL Transition Plan Frequently Asked Questions
author: Mike Milinkovich
is_deprecated: true
keywords:
  - cpl
  - epl
  - license
  - transition
  - faq
---

## For informational purposes only

This FAQ provides answers to commonly asked questions related to the transition of Eclipse projects from the [Common Public License version 1.0](http://www.ibm.com/developerworks/library/os-cpl.html) to the [Eclipse Public License (EPL)](/org/documents/epl-v10.php). Details can be found in the [CPL to EPL Transition Plan](/legal/cpl2epl/cpl2epl-transition-plan.pdf) (.pdf).

This FAQ is provided for informational purposes only. It is not part of, nor does it modify, amend, or supplement the terms of the CPL or EPL. The CPL and EPL are legal agreements that govern the rights granted to material licensed under it, so please read them carefully. This FAQ should not be regarded as legal advice. If you need legal advice, you must contact your own lawyer. This document was last updated on August 26, 2004.

1. **Why is the Eclipse Foundation changing licenses?**  
    The EPL was written specifically for the [Eclipse Foundation](/org/). First, it changes the Agreement Steward, formerly IBM for the CPL, to now be the Eclipse Foundation for the EPL. Second, it addresses concerns that some Eclipse Foundation members had with a clause in the CPL that deals with possible patent litigation. Many members and prospective members had concerns with this clause, and viewed it as overly broad. For more information on this please refer to the [EPL FAQ](/legal/epl/faq/).
2. **Can you summarize the plan briefly?**  
    The plan is to license the current release of each Eclipse project, together with any maintenance updates, under both the CPL and EPL for approximately 6-8 months. This transition period will be roughly aligned with major releases of the Eclipse Platform. This will give the community adequate time to adjust to the changes.
  
3. **What if a project can’t meet the timelines in the plan?**  
    If any projects find the timeframes to be impractical or feel the plan imposes an unreasonable burden for other reasons, they should request an exception to the plan from the EMO. The EMO is prepared to work with projects, and with the Eclipse community generally, to mitigate the impact of the license transition process.
  
4. **How will inbound contributions be handled?**  
    Under the plan, the [Web Site Terms of Use](/legal/terms-of-use/) will be changed to specify that by default, inbound contributions will be contributed under both EPL and CPL during the transition period. This is necessary to support dual licensed maintenance streams of current releases.
  
5. **What if I want to contribute code under only the EPL or CPL to a dual licensed stream?**  
    Contributions under a single license will be possible but not encouraged and will be handled as special cases (similar to the process currently used by the PMCs and EMO to deal with third party code contributed under a license other than the CPL). Approval requires a strong technical argument for including the code, explaining what the functionality is, why it is needed, and why other options that fit the dual license framework are infeasible.
  
6. **What about previous releases like Eclipse Platform 2.x?**  
    Past release streams that were licensed under CPL only and are still in use (e.g. Eclipse 2.x), will continue to be supported as CPL only.
  
7. **Will new development streams be dual licensed also?**  
    No, all new development streams going forward (e.g. Eclipse 3.1) will operate under EPL only.
  
8. **When will the re-licensing process be completed and the transition period end?**  
    This is a two-stage process, with two important dates. The plan for the re-licensing process is for it to be completed for all projects by December 31 2004. By that date, and for every project, CPL only development will cease, the current release will be offered under dual license, and new development will be EPL only. At some **switchover date** (currently expected to be in 1H05), all Eclipse development (including maintenance) will move to EPL only. The exact date of the switchover will depend on individual project schedules.
  
9. **When will we see EPL versions of major Eclipse projects?**  
    For now, major projects will prepare dual licensed releases. The Eclipse Foundation will be contacting all contributors to obtain their agreement to publish under both EPL and CPL.
  
10. **What is the Re-Licensing Committee?**  
    The Eclipse Foundation Board of Directors has created a committee called the Re-Licensing Committee to monitor the progress of the transition.
  
11. **I am a committer and I received an email asking me to identify contributors whose code I committed. Why am I getting this?**  
    Contributors to an Eclipse project provided their contribution under the CPL. We are seeking their agreement to dual license their contribution under both the CPL and EPL. To facilitate this process Eclipse is asking all committers to identify the contributors they personally know about. We would like to get their contact information (employer, address, etc.), and also would like to know what they worked on and roughly how much code they contributed. All of this will help us to first, decide exactly who needs to be contacted and second, collect the required agreements.
  
12. **What is the Eclipse Foundation going to do with the personal data it is collecting?**  
    We intend to build a contributor database so that in future we can do a better job of managing and recognizing contributions.
  
13. **What is an "individual committer"?**  
    Most committers working on Eclipse projects currently are employed by an Eclipse Member Organization, and are working on Eclipse in the course of their regular employment. Individual committers are those who fall outside this category. If the individual committer personally owns his/her contributions, then he/she may simply agree to re-contribute under the EPL. If some other entity owns the contribution, such as their employer, then he/she must obtain the permission of that entity to re-license the contribution.
  
14. **Once all the required agreements are obtained, what options does a project have to actually implement the re-licensing for projects/builds that will be licensed under the EPL?**  
    The Eclipse Foundation is currently suggesting projects use one of two options. Option #1 is to modify **all files including source files** to change all legal notices, abouts, feature licenses, feature update licenses, and the Software User Agreement, to refer to "EPL"; rather than "CPL" but only for those projects/builds that will be licensed under the EPL.  For those projects/builds that will be licensed under the CPL, no changes are required. Option #2 is similar to Option #1 except **source files will NOT BE UPDATED**. This means that the CVS repository and source builds for the stream will contain source files with effectively "incorrect" license notices that will be "overridden" by the Abouts, Software User Agreement and other legal documentation. The use of this mechanism and the status of each project will be well publicized and documented on the website
  
15. **What if I’m still confused about what my options are with respect to the re-licensing process?**  
    Contact your PMC Lead or the EMO, they will help you sort things out.
  
16. **What if a project can’t get this all done by the December 31 deadline?**  
    The Re-Licensing Committee has the authority to approve exceptions to the plan, on the recommendation of the EMO, that would allow a project more time. The criteria for considering an exception are: the size of the project; cost to completely change all source files; project schedules; available resources; and impact on the Eclipse community.
