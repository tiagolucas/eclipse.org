---
title: "Eclipse Contributor Agreement"
author: Mike Milinkovich
layout: single
hide_page_title: true
keywords:
    - certificate
    - ECA
    - contributor
    - legal
    - eclipse
related:
  links:
    - text: Eclipse Contributor Agreement [[html](/legal/eca/eca.html)]
      url: /legal/eca
    - text: ECA FAQ
      url: /legal/eca/faq
    - text: Developer Certificate of Origin [[html](/legal/dco/dco.html)]
      url: /legal/dco
    - text: Legal resources
      url: /legal
    - text: Guide to legal documents
      url: /projects/handbook#legaldoc
    - text: Committer Resources
      url: /legal#Committers
    - text: Contributing via git
      url: http://wiki.eclipse.org/Development_Resources/Contributing_via_Git
  content: |
    <p>
      To complete and submit a ECA, log into the <a href="/contribute/cla/">Eclipse projects forge</a> (you will need to create an account with the Eclipse Foundation if you have not already done so); click on "Eclipse Contributor Agreement"; and Complete the form. Be sure to use the same email address when you register for the account that you intend to use when you commit to Git.
    </p>
---

{{< pages/legal/inject-content >}}
