---
title: Eclipse Foundation Trademarks
author: Wayne Beaton
keywords:
    - legal
    - Trademarks
---

The names and logos of all open source projects, working groups, specifications, and all downloadable software products, are trademarks of the Eclipse Foundation. In addition the Eclipse Foundation logo is a trademark of the Eclipse Foundation.

This document captures a list of trademarks claimed by the Eclipse Foundation.

## Trademark Use { #use }

The [Eclipse Foundation Trademark Usage Guidelines](/legal/logo-guidelines) defines the allowable use of Eclipse Foundation names, marks and logos.

Supplemental guidelines:

- [Jakarta EE Trademark Guidelines](https://jakarta.ee/legal/trademark_guidelines/)

## Trademark Disclaimers { #attribution }

Content on this web site, including the names listed on this page, may make reference to the trademarks in the following list. We disclaim any claim to trademark rights in their usage.

- Java and all Java-based trademarks are trademarks of Oracle Corporation in the United States, other countries, or both.
- Microsoft, Windows, Windows NT, and the Windows logo are trademarks of Microsoft Corporation in the United States, other countries, or both.
- UNIX is a registered trademark of The Open Group in the United States and other countries.
- Linux is the registered trademark of Linus Torvalds in the U.S. and other countries.
- Android is a trademark of Google Inc.
- Other company, product, and service names may be trademarks or service marks of others.

{{< pages/legal/trademarks-list >}}
