---
title: Eclipse Membership
keywords: [eclipse membership, become a member, membership faq, membership expire]
headline: Eclipse Membership
subtitle: |
  Supported by our member organisations, the Eclipse Foundation provides our
  community with Intellectual Property, Mentorship, Marketing, Event and IT
  Services.
links: 
    - - href: https://accounts.eclipse.org/contact/membership/ 
      - text: Contact Us About Membership
    - - href: /membership/explore-membership/
      - text: Explore Our Members
header_wrapper_class: header-default-bg-img
hide_page_title: true
hide_sidebar: true
---

{{< pages/membership/index >}}
