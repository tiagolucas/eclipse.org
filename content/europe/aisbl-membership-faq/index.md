---
title: "Eclipse Foundation AISBL Membership FAQs"
date: 2022-07-19T16:05:57-04:00
description: "The Eclipse Foundation has transitioned to Europe as part of our continued global expansion. Our goal is to grow the global ecosystem and advance strategic open source projects with international impact."
categories: []
keywords: ["eclipse", "europe", "faq"]
slug: ""
aliases: []
toc: false
draft: false
page_css_file : public/css/europe-styles.css
header_wrapper_class: "header-europe-bg-img"
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

{{< pages/europe/aisbl-membership-faq >}}