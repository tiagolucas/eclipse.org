---
title: "HiPEAC"
date: 2022-12-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/hipeac.png"
tags: ["Computer architecture","pervasive computing","ubiquitous computing","Computing","HPC","IoT","CPS","Cloud","Edge","ecosystem"]
homepage: "https://www.hipeac.net/"
facebook: ""
linkedin: "https://www.linkedin.com/company/hipeac/"
twitter: "https://twitter.com/hipeac"
youtube: "https://www.youtube.com/@hipeac"
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "HPC/IoT/Cloud (CSA)"
summary: "High Performance, Edge And Cloud computing"
---

The objective of HiPEAC is to stimulate and reinforce the development of the dynamic European computing ecosystem that supports
the digital transformation of Europe. It does so by guiding the future research and innovation of key digital, enabling, and emerging
technologies, sectors, and value chains. The longer term goal is to strengthen European leadership in the global data economy and
to accelerate and steer the digital and green transitions through human-centred technologies and innovations. This will be achieved
via mobilising and connecting European partnerships and stakeholders to be involved in the research, innovation and development
of computing and systems technologies. They will provide roadmaps supporting the creation of next-generation computing
technologies, infrastructures, and service platforms.

The key aim is to support and contribute to rapid technological development, market uptake and digital autonomy for Europe in
advanced digital technology (hardware and software) and applications across the whole European digital value chain. HiPEAC will do
this by connecting and upscaling existing initiatives and efforts, by involving the key stakeholders, and by improving the conditions
for large-scale market deployment. The next-generation computing and systems technologies and applications developed will
increase European autonomy in the data economy. This is required to support future hyper-distributed applications and provide new
opportunities for further disruptive digital transformation of the economy and society, new business models, economic growth, and
job creation.

The HiPEAC CSA proposal directly addresses the research, innovation, and development of next generation computing and systems
technologies and applications. The overall goal is to support the European value chains and value networks in computing and
systems technologies across the computing continuum from cloud to edge computing to the Internet of Things (IoT).


This project is running from December 2022 to December 2024.

---

## Consortium
* UNIVERSITEIT GENT Belgium
* ARM LIMITED United Kingdom
* BARCELONA SUPERCOMPUTING CENTER-CENTRO NACIONAL DE SUPERCOMPUTACION Spain
* COMMISSARIAT A L ENERGIE ATOMIQUE ET AUX ENERGIES ALTERNATIVES FR
* CLOUDFERRO SP ZOO PL
* ECLIPSE FOUNDATION EUROPE GMBH DE
* IDC ITALIA SRL IT
* INSTITUT NATIONAL DE RECHERCHE EN INFORMATIQUE ET AUTOMATIQUE FR
* ARTEMISIA VERENIGING NL
* RHEINISCH-WESTFAELISCHE TECHNISCHE HOCHSCHULE AACHEN DE
* SINTEF AS NO
* THALES FR

