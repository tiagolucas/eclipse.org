---
title: "ENACT"
date: 2024-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/enact.png"
tags: ["Information science", "Bioinformatics", "Cognitive Computing Continuum", "AI", "Dynamic Graph Models", "Graph Neural Networks", "Deep Reinforcement Learning", "Hyper-distributed applications", "Application Programming Model", "Zero-touch", "SDK"]
homepage: ""
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "Cloud-Edge, Energy"
summary: "Adaptive Scheduling and Deployments of Data Intensive Workloads on Energy Efficient Edge and Cloud to Cloud Continuum"
---

ENACT develops cutting-edge techniques and technology solutions to realise a Cognitive Computing Continuum (CCC) that can address the needs for optimal (edge and cloud) resource management and dynamic scaling, elasticity, and portability of hyper-distributed data-intensive applications. At infrastructure level, the project brings visibility to distributed edge and cloud resources by developing Dynamic Graph Models capable of capturing and visualising the real-time and historic status information, connectivity types, dependencies, energy consumption etc. from diverse edge and cloud resources. The graph models are used by AI (Graph Neural Networks - GNN) models and Deep Reinforcement Learning (DRL) agents to suggest the optimal deployment configurations for hyper distributed applications considering their specific needs. The AI (GNN and DRL) models are packaged as an intelligent decision-making engine that can replace the scheduling component of open-source solutions such as KubeEdge. This will enable real-time and predictive management of distributed infrastructure and applications. To take full advantage of the potential (compute, storage, energy efficiency etc) opportunities in the CCC, ENACT will develop an innovative Application Programming Model (APM).

The APM will support the development of distributed platform agnostic applications, capable of self-determining their optimal deployment and optimal execution configurations while taking advantage of diverse resources in the CCC. An SDK to develop APM-based distributed applications will be developed. Moreover, services for automatic (zero-touch provisioning-based) resource configuration and (telemetry) data collections are developed to help design and update dynamic graph models. ENACT CCC solutions will be validated in 3 use-cases with challenging resource and application requirements. International collaboration is planned as Japan Productivity Center has committed to support with knowledge sharing.


This project is running from January 2024 - December 2026.

---

## Consortium
* Centre for Research and Technology Hellas	(Coordinator) - Greece
* Universitat Politecnica de Valencia - Spain
* Information Catalyst SL - Spain
* Eclipse Foundation Europe GmbH	 - Germany
* MOG Technologies SA - Portugal
* Unparallel Innovation LDA - Portugal
* DunavNet - Ireland
* Digital Systems 4.0 - Bulgaria
* Four Dot Infinity - Greece
* Asociacion de empresas technologicas Innovalia - Spain
* SIEMENS SRL - Romania
* Institute of Communication & Computer Systems “ICCS” - Greece
* University of Western Macedonia - Greece
* Arctur computer engineering d.o.o. - Slovenia
* Fujitsu Technology Solutions SPZOO	 - Poland
* Sociedad de Proyectos para la Transformación Digital - Spain

