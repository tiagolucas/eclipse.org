---
title: "NOUS"
date: 2024-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/nous.png"
tags: ["Data curation", "Linked open data", "Cloud architecture & services", "HPC", "Quantum computing", "Edge computing", "Distributed/Federated concepts", "AI", "ML",
"Blockchain", "Data management", "Data Spaces", "sharing", "life cycle & economy", "Cybersecurity"]
homepage: "https://nous-project.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/nous-eu/"
twitter: "https://twitter.com/nous_project"
youtube: "https://www.youtube.com/channel/UCls7_vS0APC7TKnypvz3KHA"
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "Cloud-Edge, Data Spaces"
summary: "Empowering Europe\\'s Data Future"
---

stands for: "a catalyst for EuropeaN ClOUd Services in the era of data spaces, high-performance and edge computing"

NOUS will develop the architecture of a European Cloud Service that allows computational and data storage resources to be used
from edge devices as well as supercomputers, through the HPC network, and Quantum Computers. NOUS will be an Infrastructure-as-
a-Service (IaaS)/Platform-as-a-Service (PaaS) cloud provider, harnessing edge computing and decentralisation paradigms to
incorporate a wide array of devices and machines in its computational flow to provide leaps in Europe’s capability to process vast
amounts of data.

The pipeline of the NOUS in the project will include three types of components: i) computational components that are responsible for
executing computations, ii) edge components that are responsible for communicating with edge devices (such as IoT sensors/
actuators/ devices), iii) data storage components that are responsible for data storage and storage management. Components are
researched individually, expecting to yield breakthroughs, and jointly, to create the architecture and cloud-level services such as
syndication with other platforms and virtual labs. The project has defined 4 use-cases that will allow the testing of the developed
technologies in real-world scenarios that industry leaders face.

The NOUS architecture will be made open source to allow the capitalisation by companies and organisations. Furthermore, a set of
workshops and collaboration activities is envisioned with Data Spaces Support Centre, Gaia-X, FIWARE and EOSC powered by a strong
consortium of 21 partners from 11 European countries.


This project is running from January 2024 - December 2026.

---

## Consortium
* FUNDACION INSTITUTO INTERNACIONAL DE INVESTIGACI (Coordinator) - Spain
* AETHON ENGINEERING SINGLE MEMBER PC - Greece 
* UNIVERSIDAD DE SALAMANCA - Spain 
* NATIONAL CENTER FOR SCIENTIFIC RESEARCH "DEMOKRIT - Greece 
* CONSORZIO INTERUNIVERSITARIO PER L'OTTIMIZZAZIONE - Italy 
* POLITECNICO DI TORINO (Affiliated) - Italy
* UNIVERSITA DEGLI STUDI DI MODENA E REGGIO EMILIA (Affiliated) - Italy
* TELECOM ITALIA SPA- Italy
* NETCOMPANY-INTRASOFT SA - Luxembourg  
* IOTAM INTERNET OF THINGS APPLICATIONS AND MULTI L - Cyprus
* UNIVERSITA DI PISA - Italy 
* UNPARALLEL INNOVATION LDA - Portugal 
* KATHOLIEKE UNIVERSITEIT LEUVEN - Belgium 
* Hewlett-Packard Hellas EPE  - Greece
* ECLIPSE FOUNDATION EUROPE GMBH - Germany 
* F6S NETWORK IRELAND LIMITED - Ireland 
* DIMOSIA EPICHEIRISI ILEKTRISMOU ANONYMI ETAIREIA - Greece
* ARCTUR RACUNALNISKI INZENIRING DOO - Slovenia 
* AEGIS IT RESEARCH GMBH - Germany 
* CS GROUP-FRANCE - France 
* INESC TEC - INSTITUTO DE ENGENHARIADE SISTEMAS - Portugal
