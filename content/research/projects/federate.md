---
title: "FEDERATE"
date: 2023-10-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/federate.png"
tags: ["Software defined vehicle", "SDV",  "Open Source", "SDV Ecosystem", "SDV HW Platform", "Coordination", "Support", "CSA"]
homepage: "https://federate-sdv.eu/"
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
zenodo:  ""
funding_bodies: ["chips-ju"]
eclipse_projects: []
project_topic: "SDV, CSA"
summary: "SoFtwarE DefinEd vehicle suppoRt And coordinaTion projEct"
---

The automotive industry faces tremendous challenges in addressing decarbonization through electrification, developing future
solutions for inclusive, safe and affordable mobility. Many of these changes require a radical re-thinking of existing development
processes, with the share of software in modern mobility solutions continuously increasing. The rising importance of the software
layer results in the so-called software-defined vehicle (SDV). 

Automotive software will be developed and adapted in continuous
cycles. Therefore an abstraction from the underlying hardware needs to be implemented. As a result, the automotive industry is
transitioning to an agile software development process. The dramatic increase of software and complexity, along with the advances
of international competition in this domain, calls for an approach, in which non-differentiating software is developed jointly as open
source. To address these challenges, the EU, together with industry, governments, and research institutions, have launched the
European SDV Ecosystem. To turn this into reality, this proposal outlines the vision and activities for a Coordination and Support
Action. 

FEDERATE (Software-Defined Vehicle Support and Coordination Project) aims to bring together all relevant stakeholders to
accelerate the development of an SDV Ecosystem, to foster a vibrant European community and orchestrate the SDV R&D&I activities.
The consortium of FEDERATE is formed by major European OEMs, automotive tiers, semiconductor companies, relevant industry
associations and industrial SDV initiatives, including the Eclipse SDV WG, and supported by a scientific board. FEDERATE will work
towards a common understanding on the vision of the SDV program and create an orchestrated advice for current and future projects
in the SDV program. In addition, recommendations for future calls are prepared in alignment with a Roadmap and Joint Vision
Document for accelerated SDV R&D&I, created as part of the CSA.


This project is running from October 2023 - September 2026.

---

## Consortium
* AVL LIST GMBH	AT
* BAYERISCHE MOTOREN WERKE AKTIENGESELLSCHAFT	DE
* MERCEDES-BENZ AG	DE
* RENAULT SAS	FR
* CARIAD SE	DE
* FORD OTOMOTIV SANAYI ANONIM SIRKETI	TR
* ROBERT BOSCH GMBH	DE
* CONTINENTAL AUTOMOTIVE TECHNOLOGIES GMBH	DE
* CONTINENTAL AUTOMOTIVE FRANCE SAS	FR
* ELEKTROBIT AUTOMOTIVE GMBH	DE
* TTTECH AUTO AG	AT
* VALEO COMFORT AND DRIVING ASSISTANCE	FR
* ZF FRIEDRICHSHAFEN AG	DE
* FAURECIA SERVICES GROUPE	FR
* INFINEON TECHNOLOGIES AG	DE
* NXP SEMICONDUCTORS NETHERLANDS BV	NL
* STMICROELECTRONICS FRANCE	FR
* AVL DEUTSCHLAND GMBH	DE
* ETAS GMBH	DE
* VERUM SOFTWARE TOOLS B.V.	NL
* OULUN YLIOPISTO	FI
* RHEINISCH-WESTFAELISCHE TECHNISCHE HOCHSCHULE 	DE
* TECHNISCHE UNIVERSITAET MUENCHEN	DE
* VIRTUAL VEHICLE RESEARCH GMBH	AT
* VDI/VDE INNOVATION + TECHNIK GMBH	DE
* ECLIPSE FOUNDATION EUROPE GMBH	DE
* UAB METIS BALTIC	LT
* FZI FORSCHUNGSZENTRUM INFORMATIK	DE
* VECTOR INFORMATIK GMBH	DE