---
title: "NEMO"
date: 2022-09-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/nemo.png"
tags: ["Edge Computing", "Cloud-Edge continuum", "Meta-orchestration", "AI at Edge"]
homepage: ""
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["horizon2020"]
eclipse_projects: []
project_topic: "Cloud-Edge"
summary: "Next Generation Meta Operating System"
---

NEMO aims to establish itself as the game changer of AIoT-Edge-Cloud Continuum by introducing an open source, flexible, adaptable, cybersecure and multi-technology meta-Operating System, sustainable during and after the end of the project, via the Eclipse foundation (NEMO consortium member). To achieve technology maturity and massive adoption, NEMO will not “reinvent the wheel”, but leverage and interface existing systems, technologies and Open Standards, and introduce novel concepts, tools, testing facilities/ Living Labs and engagement campaigns to go beyond today’s state of the art, make breakthrough research and create sustainable innovation, already within the project lifetime.
NEMO will introduce innovations at different layers of the protocol stack, enabling on-device Cybersecure Federated ML/DRL, deliver time-triggered (TSN) multipath ad-hoc/hybrid self-organized and zero-delay failback/self-healing multi-cloud clusters, multi- technology Secure Execution Environment and on-Service Level Objectives meta-Orchestrator, Plugin and Apps Lifecycle Management and Intent Based programming tools. Moreover, NEMO will be “by design” and “by innovation” cybersecure and trusted adopting state of the art mechanisms such as Mutual TLS and Digital Identity Attestation.


NEMO will be validated in 5 most prominent industrial sectors (i.e. Farming, Energy, Mobility/City, Industry 4.0 and Media/XR) and 8 use cases in 5 +1 Living Labs, utilizing more than 30 heterogenous IoT devices and real 5G infrastructure. The impact will not only safeguard EU position in data economy and applications verticals, but lower energy efficiency, reduce pesticides and CO2 footprint. Beyond Eclipse adoption, NEMO sustainability, wide acceptance and SMEs engagement will be achieved via open source ecosystems, standardization initiatives and 2 Open Calls that will provide financial support of 1,8M€ and access to NEMO Living Labs to SMEs and enlarge NEMO by at least 16 new entities.


This project is running from January 2021 - December 2023.

---

## Consortium
* Atos Spain S.A. - Spain (Coordinator)
* Atos IT Spain - Spain
* Thales Six GTS France S.A.S. - France
* Engineering-Ingegneria Informatica SPA - Italy
* Software Imagination & Vision SRL - Romania
* INTRASOFT International S.A. - Luxemburg
* Maggioli Group SPA - Italy
* Space Hellas S.A. - Greece
* Telefonica I+D - Spain
* Hellenic Telecommunications Organization S.A. - Greece
* WIND TRE SpA - Italy
* Continental Temic Microelectronic GmbH - Germany
* Entersoft S.A. - Greece
* ASM Terni SpA - Italy
* Foundation of Hellenic World - Greece
* Cumucore Oy - Finland
* Emotion s.r.l. - Italy
* Novoville Ltd - UK
* Synelixis Solutions S.A. - Greece
* Sphynx Technology Solutions - Switzerland
* AEGIS IT Research GmbH - Germany
* Sorbonne Université - France
* Universidad Politécnica De Madrid - Spain
* Institute of Communication and Computer Systems - Greece
* Rheinisch-Westfälische Technische Hochschule Aachen - Germany
* Eclipse Foundation Europe GmbH - Germany

