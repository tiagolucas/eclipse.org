---
title: "Research @ Eclipse Foundation"
cascade:
  # header
  header_wrapper_class: "header-research-bg-img small-jumbotron-subtitle"
  headline: "Research @ Eclipse Foundation"
  jumbotron_class: "col-md-24"
  custom_jumbotron_class: "research-custom-jumbotron col-sm-24"
  custom_jumbotron: |
    <div class="research-custom-jumbotron row">
      <p class="col-md-14">
        Business-friendly open source and open innovation underpin
        exploitation, community building, and dissemination strategies for
        European projects
      </p>
    </div>

  hide_call_for_action : true
  hide_sidebar: true
  hide_page_title: true

  # metadata
  description: Business-friendly open source and open innovation underpins exploitation, community building and dissemination strategy for European projects.
  keywords: ["eclipse", "research"]
  seo_title_suffix : " | Research | The Eclipse Foundation"

  page_css_file: /public/css/research-styles.css
  date: 2019-09-10T15:50:25-04:00
  hide_page_title: true
  show_featured_story: false
  show_featured_footer: false
  container: "container-fluid"
---
{{< grid/section-container id="esaam" class="text-center row-light padding-top-40 padding-bottom-40" isMarkdown="false">}}
		<a href="https://eclipse.org/events/2024/oc4research"  target="_blank">
			<img 
                class="img img-responsive center-block" 
                src="/research/images/esaam-adv.jpeg" 
                alt="eSAAM 2024 in Mainz on 22 October 2024. Co-organised by the Eclipse Foundation, Investigate to Innovate, University of Macedonia, and Centre for Research &amp; Technology Hellas"
            >
	   	</a>
{{</ grid/section-container >}}

{{< pages/research/home-section-header >}}

{{< pages/research/home-section-info >}}

{{< pages/research/home-section-opportunities >}}

{{< pages/research/home-section-organizations >}}

{{< grid/section-container id="projects" class="row-gray padding-top-40 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="text-center margin-bottom-40">The Eclipse Foundation is a Partner in these Projects</h2>
    {{< eclipsefdn_projects is_static_source="true" types="projects" url="/research/projects/index.json" templateId="tpl-projects-item-research" display_view_more="false" >}}
{{</ grid/section-container >}}

{{< pages/research/collaboration-section >}}

{{< pages/research/newsroom-section id="collaborations" working_group="research" class="margin-bottom-30" resource_class="col-md-5 newsroom-resource-card newsroom-resource-card-match-height match-height-item" >}}

{{< pages/research/home-section-contact >}}

{{< mustache_js template-id="tpl-projects-item-research" path="/js/src/templates/research/tpl-projects-item-research.mustache" >}}
