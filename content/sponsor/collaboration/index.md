---
title:  Sponsor a Collaboration
hide_sidebar: true
keywords: ["Sponsor working group", "sponsor interest group", "sponsor industry collaboration"]
description: Show your direct support for the communities that matter to you.
container: container center-align-h1
page_css_file : /public/css/collaboration.css
---

If you are looking to support a working group, without committing to membership, you can become a sponsor! As a sponsor, you can donate on a one-time or recurring annual basis to support working group initiatives that matter to your organisation.

Anyone can contribute funds to the Eclipse Foundation:

{{< pages/sponsor/collaboration >}}

All sponsored funds will be included in the working group’s annual budget, which is under the direction of the working group’s steering committee. Sponsors are encouraged but not required to indicate their non-binding commitment to future sponsorships to promote the stability of the working group, and to allow the steering committee to plan for multi-year initiatives.

If you have questions about sponsorship, reach out to sponsor@eclipse.org.