---
title: Collaboration Has No Boundaries
section_id: collaboration-has-no-boundaries
weight: 5
quote: |
    We're able to partner with Thales on projects for the long term. We're also able to build on our experiences
    and involvement in the Eclipse Foundation to act as a middleman who can help and advise Thales
    about opportunities to enhance their solutions.
quote_author: |
    Cédric Brun,

    CEO, Obeo
---

In a standalone open source association, collaboration opportunities are typically limited to the members of the association.

At the Eclipse Foundation, there are endless opportunities for ecosystem members and entire ecosystems to openly collaborate on
shared goals across technologies and industry focus areas. There’s no need to negotiate individual agreements or manage multiple
highly diverse sets of requirements and rules.

At the ecosystem level, a number of working groups are collaborating to strengthen their individual efforts, including:

- [Jakarta EE](https://jakarta.ee/) and [MicroProfile](https://microprofile.io/) Working Groups
- [Eclipse IoT](https://iot.eclipse.org/) and [Eclipse Edge Native](https://edgenative.eclipse.org/) Working Groups

Within these very diverse ecosystems, members ranging from major multinational players to small, entrepreneurial organisations have
discovered the potential to combine their respective technologies. For example, Obeo, a Strategic Member of the Eclipse Foundation
with a few dozen employees, is partnering with Thales, a multinational corporation with tens of thousands of employees. Both companies
contribute to numerous Eclipse Foundation projects.
