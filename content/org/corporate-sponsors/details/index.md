---
title: Become an Eclipse Corporate Sponsor
author: "Eclipse Foundation, Inc."
keywords: ["Eclipse Foundation", "corporate sponsorship", "sponsor", "eclipse"]
is_deprecated: true
related:
  title: "Supporting Eclipse"
  links:
    - text: "Corporate Sponsors"
      url: "/org/corporate-sponsors/"
    - text: "Eclipse Members"
      url: "/membership/explore-membership/"
---

The Eclipse Foundation relies on the support of our members and contributions from the user community to service and grow the Eclipse ecosystem. We encourage companies that make extensive use of Eclipse technology to support the Eclipse community through the Eclipse Corporate Sponsor Program. The generous support of these Corporate Sponsors allows the Foundation to provide world-class support for the Eclipse open source projects.

A company may become a corporate sponsor by making a financial contribution or an in-kind contribution of goods and services to the Eclipse Foundation. There are three tiers of sponsorship: 1) Platinum, 2) Gold and 3) Silver; each tier representing the level of annual sponsorship to the Eclipse Foundation.

## Platinum Sponsorship { .h4 }

* Annual contribution of at least US$100,000
* Corporate logo placed on the Eclipse Foundation Sponsor page.
* Joint-press release issued between the Eclipse Foundation and Platinum sponsor.

## Gold Sponsorship { .h4 }

* Annual contribution of at least US$25,000
* Corporate logo recognized on the Eclipse Foundation Sponsor page.
* Supporting press quote from the Eclipse Foundation for sponsor announcement.

## Silver Sponsorship { .h4 }

* Annual contribution of at least US$5000
* Corporate logo recognized on the Eclipse Foundation Sponsor page.

### How to Become a Corporate Sponsor

* #### Step 1

    Determine the level of sponsorship your company would like to make.

* #### Step 2
  
    Contact the [Eclipse Foundation](mailto:membership@eclipse.org) to determine method of payment. In-kind contributions will require previous agreement from the Eclipse Foundation. Please note that is Eclipse Foundation is a not-for-profit corporation. Contributions or gifts to the Eclipse Foundation Inc. are not tax deductible as charitable contributions.

* #### Step 3

    Corporate sponsors, by signing our [logo agreement](/org/documents/Eclipse%20Logo%20Agreement%20%202008_04_30%20final.pdf), agree to have their corporate logo listed on the Eclipse Corporate Sponsor page. If you do not want your logo listed, please inform the Eclipse Foundation.

### Other Ways to Support Eclipse

The Eclipse Foundation is supported by our member companies through their contributions to the Eclipse projects and membership dues. If your company would like to [become more involved in the Eclipse community](/membership/), please explore the different membership options. Individuals may also contributed to the Eclipse Foundation through the ['Friends of Eclipse'](/donate/) program.
