---
title: Press Release Guidelines
hide_sidebar: false
author: "Ian Skerrett"
keywords: ["Eclipse Foundation", "press release", "guidelines", "Eclipse Foundation guidelines"]
---

* [Introduction](./#introduction)
* [Goals of the Guidelines](./#goals)
* [Events That May Trigger a Press Release](./#press-release-triggers)
* [Requesting a Press Release](./#requesting-a-press-release)
* [General Guidelines for Press Releases Issued by the Eclipse Foundation](./#general-guidelines)

## Introduction { #introduction }

The Eclipse Foundation has been very successful in developing strong press relationships resulting in community-related news being frequently reported. As the number of projects, working groups, and members grow, media interest in the Eclipse ecosystem is also growing. Therefore, to maintain our strong relationship with the news media and to support the public relations efforts of our member organizations and projects, we have developed a set of guidelines on the issuance of press releases from and involving the Eclipse Foundation.

Please refer to the following guidelines when preparing a press release that incorporates Eclipse Foundation names or trademarks or involves the Eclipse Foundation in any way. For press release approval, or further questions, please contact the Foundation&rsquo;s marketing team at [pr@eclipse-foundation.org](mailto:pr@eclipse-foundation.org).

## Goals of the Guidelines { #goals }

* Maintain the positive image of the Eclipse Foundation with the journalists, editors, influencers, and publications that cover the Foundation.
* Ensure the Eclipse Foundation is viewed positively in the greater software industry.
* Support member&rsquo;s desires to publicize their involvement with the Eclipse Foundation and their participation in the Eclipse community.
* Ensure a level of consistency and professionalism regarding public relations efforts around the Eclipse Foundation.
* Maintain a level of momentum around the Eclipse Foundation through regular news updates that demonstrates the activity and vitality of the Eclipse community.

## Events That May Trigger a Press Release { #press-release-triggers }

There are different events that may trigger a press release related to the Eclipse Foundation. This section provides the guidelines of what the Foundation involvement may be in those press releases.

### Company Joins the Eclipse Foundation as a Member

When a company joins the Eclipse Foundation, the company is encouraged to issue their own press release announcing their membership in the Foundation. The Eclipse Foundation will support this press release by providing a supporting quote. The actual release is issued by the member company. The Foundation will link to the new member press release from the eclipse.org website.

### Company Joins the Eclipse Foundation as a Strategic Member

When a company joins the Eclipse Foundation as a Strategic Member and hence becomes a member of the Board of Directors, the Eclipse Foundation would like to do a joint press release with the company. The decision to do a joint release is with the member company. If a joint release is desired, the Eclipse Foundation will participate in the press outreach activities. If the company would like to issue their own release, the Eclipse Foundation would be willing to provide a supporting quote. The Foundation will link to the new member press release from the eclipse.org website.

### Company Releases a Commercial Product Based on/Integrated with Eclipse-Governed Technologies or a Product Certified Compatible with an Eclipse Foundation Specification

If a member company issues a press release about a product that is based on or is integrated with Eclipse-governed technologies or is certified compatible with an Eclipse Foundation specification, the Eclipse Foundation will support this press release by providing a supporting quote. The actual release is issued by the member company. The Foundation will link to the new product press release from the eclipse.org website.

### Company Proposes a New Eclipse Project

When a company proposes a new Eclipse project, they may wish to issue a press release. The Eclipse Foundation will support this press release with a supporting quote. It is important that the press release positions the news of the project as a &lsquo;project proposal&rsquo;. This is to respect the Eclipse development process that requires a 30-day period of feedback before a project can be considered for creation. The Eclipse Foundation will link to the new member press release from the eclipse.org website.

### New Project is Created at or Transitioned to the Eclipse Foundation

When a new project is created, or an incubator project is graduated, the Eclipse Foundation and the company that initially proposed the project may decide to issue a press release. This would be a joint release between the company and the Eclipse Foundation. The main message of the release needs to reflect the Eclipse Foundation-specific news and should not contain a strong corporate message. If appropriate, the Eclipse Foundation will participate in the press outreach activities. The press release would be posted on the eclipse.org web site.

### New Working Group is Created at or Transitioned to the Eclipse Foundation

When a new working group is created, the Eclipse Foundation may decide to issue a press release in collaboration with the working group. The messaging of the release needs to reflect the working group&rsquo;s vision, scope, priorities, and related Eclipse projects. If appropriate, the Eclipse Foundation and media spokespersons for the founding member organizations would participate in the press outreach activities. The press release would be posted on the eclipse.org website.

### Project Oriented Press Releases

Press releases to announce news about a project, (e.g. a new version or significant milestone such as industry adoption news) are issued by the Eclipse Foundation. All projects are invited to propose potential press releases. The Project/PMC Lead is required to approve any project-specific press release and act as a spokesperson for the release. To encourage a vendor neutral community, companies are encouraged to not issue press releases regarding a specific Eclipse project.

The Eclipse Foundation will not issue press releases announcing new versions of a technology or incubation projects. In general, technology and incubation projects are not intended for production use; therefore the promotion of these projects should be focused within the existing Eclipse community. New releases of technology and incubation projects can be promoted via the elipse.org website and forums.

### Foundation News Releases

The Eclipse Foundation may from time to time issue press releases to support certain events or strategies. For example, press releases announcing EclipseCon, participation in a trade show or momentum in the Eclipse community.

## Requesting a Press Release { #requesting-a-press-release}

Projects, working groups, and member companies can request the issuance of a press release by completing the Eclipse Foundation [Press Release Request Form](https://forms.gle/Lcpv2L212cdhanfn7). Please use this form to submit requests for the Eclipse Foundation to produce press releases, announcements, blogs, quotes, or other publicity about your Eclipse community-related news. Once submitted, a member of the Foundation&rsquo;s Marketing team will be in touch to confirm your request and take the next steps.

## General Guidelines for Press Releases Issued by the Eclipse Foundation { #general-guidelines }

* In general, the Eclipse Foundation tries not to overstate the capabilities of our projects and avoid obvious hyperbole-filled statements. Ours is an open source community that has a strong developer focus. Over-hyped statements are generally viewed negatively by the community.
* Usage of Eclipse Foundation names, marks and logos must adhere to the [Eclipse Foundation Trademark Usage Guidelines](/legal/logo-guidelines/).
* The Eclipse Foundation avoids any statements that may be viewed as competitive to a member company.
* A company leading an Eclipse Foundation open source project should not claim ownership of an open source project. For instance statements such as _ABC Company&rsquo;s Eclipse favourite project has announced…_ should be avoided. The Eclipse Foundation is a vendor-neutral open source community, so ownership claims go against this philosophy. Reinforcing a company&rsquo;s leadership in a project via a supporting quote is fine.
* Whenever possible, a press release should promote the fact that the Eclipse project is being adopted by a wide variety of commercial products and stress the open source nature of the project. This is to reinforce the message that Eclipse projects and technologies are adopted broadly for building commercial products.
* Press releases announcing future capabilities or new project versions that will be available more than 30 days after the press release should be avoided. The philosophy is to only promote what is available to the community today.
* Mike Milinkovich, Executive Director of the Eclipse Foundation, serves as the primary spokesperson and is responsible for all quotes attributed to the Eclipse Foundation in press communications. In circumstances when the Executive Director is not available, or with the prior approval of the Executive Director, alternative spokespersons may address the press. This approach ensures consistent messaging and upholds our commitment to transparent communication. Alternative spokespeople for the Eclipse Foundation include Thabang Mashologu, Vice President of Marketing and Community Programs; Gaël Blondelle, Chief Membership Officer; Michael Plagge, VP of Ecosystem Development; and Mikael Barbero, Head of Security.
* For project-specific press releases, the Project/PMC Lead may act as the spokesperson and provide quotes attributed in the press releases.
* All press releases from the Eclipse Foundation must be approved by the VP of Marketing and Community Programs and the Executive Director of the Eclipse Foundation.
* The &lsquo;boilerplate&rsquo; description of the Eclipse Foundation to be used in press releases is as follows:

    > **About the Eclipse Foundation**
    >
    > _The Eclipse Foundation provides our global community of individuals and organisations with a business-friendly environment for open source software collaboration and innovation. We host the [Eclipse IDE](https://eclipseide.org/), [Adoptium](https://adoptium.net/), [Software Defined Vehicle](https://sdv.eclipse.org/), [Jakarta EE](https://jakarta.ee/), and over 410+ open source [projects](https://www.eclipse.org/projects/), including runtimes, tools, specifications, and frameworks for cloud and edge applications, IoT, AI, automotive, systems engineering, open processor designs, and many others. Headquartered in Brussels, Belgium, the Eclipse Foundation is an international non-profit association supported by over 360 [members](https://www.eclipse.org/membership/). To learn more, follow us on social media [@eclipsefdn](https://x.com/EclipseFdn), [LinkedIn](https://www.linkedin.com/company/eclipse-foundation/) or visit [eclipse.org](https://www.eclipse.org/)._
  
* It is expected that as the Eclipse community grows and matures, what is considered newsworthy about Eclipse Foundation will evolve. Therefore, these guidelines will evolve and be updated from time to time. It is also expected that certain events may require the Eclipse Foundation to issue a press release that is not covered by or contradict these guidelines. In that case, the Executive Director of the Eclipse Foundation will use his discretion and best judgment for issuing the press release.
