---
title: "Staff"
date: 2024-07-08
tags: ["staff", "employees"]
---

The Eclipse Foundation is fortunate to have some very talented people working
full-time on behalf of the Eclipse community.\
Below is the list of our staff.

Eclipse email addresses all follow the
`firstname.lastname@eclipse-foundation.org` convention.

{{< pages/org/foundation/staff/leadership >}}
{{< pages/org/foundation/staff/supporting_our_communities >}}

