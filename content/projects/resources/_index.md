---
title: Project Resources
keywords: ["Eclipse Foundation Development Process", "Eclipse Foundation Project Handbook", "Committer Training", "Eclipse Management Office", "Eclipse Project Support"]
hide_sidebar: true
hide_page_title: true
page_css_file: /public/css/projects/resources.css
---

{{< pages/projects/resources >}}
