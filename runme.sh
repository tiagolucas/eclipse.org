#!/bin/bash
#*******************************************************************************
#* Copyright (c) 2022 onward Eclipse Foundation and others.
#* All rights reserved. This program and the accompanying materials
#* are made available under the terms of the Eclipse Public License v2.0
#* which accompanies this distribution, and is available at
#* http://www.eclipse.org/legal/epl-v20.html
#*
#* Contributors:
#*   Matt Ward (Eclipse Foundation)
#*******************************************************************************/

umask 0022

#setup basic definitions
IMAGE="docker.io/eclipsefdn/www-eclipse-org:latest"
#podman should be a drop in replacement for docker
PODMAN="/usr/bin/podman"
CONTAINER_NAME="www-httpd"
IMAGE_TRACKER="/var/run/eclipse.org-image"
LATEST_IMAGE=""

function pull_image {
#  echo "Fetching $IMAGE"
  # -q doesn't seem to supress all output, so let's trap it in a local var
  local RETURN=$($PODMAN pull -q $IMAGE)
}

function get_image_id {
  local IMAGE_ID=$($PODMAN history --format "{{.ID}}" $IMAGE | head -1)
  echo $IMAGE_ID
}

function run_image {
  echo "Running Image $LATEST_IMAGE"
  $PODMAN run --name=$CONTAINER_NAME -d -p 18181:8080 $LATEST_IMAGE
}

#main loop starts here.
if [ -f ${IMAGE_TRACKER} ] && [ -s ${IMAGE_TRACKER} ] ; then
  RUNNING_IMAGE=$(cat $IMAGE_TRACKER)
else
  RUNNING_IMAGE=""
fi

#pull and get the image id
pull_image

LATEST_IMAGE=$(get_image_id)
if [[ $RUNNING_IMAGE == "" ]]; then
  echo "Init setting RUNNING_IMAGE=LATEST_IMAGE"
  run_image
  echo -n $LATEST_IMAGE > $IMAGE_TRACKER
elif [[ $RUNNING_IMAGE != $LATEST_IMAGE ]]; then
  echo "Latest image id is not the running image, restarting"
  $PODMAN stop $CONTAINER_NAME
  $PODMAN rm $CONTAINER_NAME
  run_image
  #update the image 'version' tracker file
  echo -n $LATEST_IMAGE > $IMAGE_TRACKER
fi
