/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { getEdition } from '../api/eclipsefdn.newsroom';
import { getProject } from '../api/eclipsefdn.projects';
import featuredCardTemplate from '../templates/home/featured-card.mustache';

const render = async () => {
  const element = document.querySelector('.eclipsefdn-featured-project-committer');
  if (!element) return;

  const [edition, error] = await getEdition();

  if (error) {
    console.error(error);
    return;
  }

  // Retrieve the project that is featured in the edition.
  const [project, projectError] = await getProject(edition.featuredProject.id);

  if (projectError) {
    console.error(`Could not retrieve project ${edition.featuredProject.id}`, projectError);
    return;
  }

  // Render three cards.
  element.innerHTML = ( 
    featuredCardTemplate({
      title: 'Community Newsletter',
      image: '/home/images/newsletter-bg.jpg',
      emphasizedText: edition.title,
      link: 'https://newsroom.eclipse.org/eclipse-newsletter',
      ctaText: 'Read More',
      class: 'featured-card-newsletter'
    }) +
    featuredCardTemplate({
      title: 'Featured Committer',
      image: edition.featuredCommitter.picture,
      emphasizedText: edition.featuredCommitter.name,
      text: edition.featuredCommitter.body, 
      link: edition.featuredCommitter.url,
      ctaText: 'Read More',
      class: 'featured-card-committer',
    }) + 
    featuredCardTemplate({
      title: 'Featured Project',
      image: project.logo,
      emphasizedText: project.name,
      link: `https://projects.eclipse.org/projects/${project.projectId}`,
      ctaText: 'Read More',
      class: 'featured-card-project',
    })
  )
}

export default { render };
