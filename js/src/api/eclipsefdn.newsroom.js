/*!
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { transformSnakeToCamel } from 'eclipsefdn-solstice-assets/js/api/utils';

const newsroomApiBaseUrl = 'https://newsroom.eclipse.org/api';

/**
  * Retrieves the latest edition from the newsroom.
  */
export const getEdition = async () => {
  try {
    const response = await fetch(`${newsroomApiBaseUrl}/edition?options[orderby][nid]=DESC&pagesize=1`);
    const data = await response.json();
    if (data.editions.length === 0) throw new Error('No edition found.');

    const edition = transformSnakeToCamel(data.editions[0]);

    return [edition, null];
  } catch (error) {
    console.error(error);

    return [null, error];
  }
}

