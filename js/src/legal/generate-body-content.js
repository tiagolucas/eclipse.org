/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import getBodyContent from "./get-body-content";

// Valid injectable pages. Mapped by URL path
const injectablePages = {
  '/legal/committer-guidelines/': '/legal/documents/html/committers-dd.html',
  '/legal/epl-2.0/': '/org/documents/epl-2.0/EPL-2.0.html',
  '/legal/epl-2.0/faq/': '/legal/documents/html/epl-2.0-faq.html',
  '/legal/faq/': '/legal/documents/html/legalfaq.html',
  '/legal/dco/': '/legal/dco/dco.html',
  '/legal/eca/': '/legal/eca/eca.html',
  '/legal/efsl/': '/legal/efsl/efsl.html',
  '/legal/tck/': '/legal/tck/tck.html',
  '/legal/epl/notice/': '/legal/epl/notice/notice.html'
}

/**
 * Injects content from a static HTML file into a div with id 'content'. Injected page is based on the current URL path.
 */
const generateBodyContent = async () => {
  const path = injectablePages[window.location.pathname];
  document.getElementById('content').innerHTML = await getBodyContent(path);
};

export default generateBodyContent();
