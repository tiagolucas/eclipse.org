/**
  * Format the blog posts in the blog list.
  *
  * @param {Element} blogListElement - The blog list element.
  */
const formatPosts = (blogListElement) => {
  const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const months = [
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 
    'August', 'September', 'October', 'November', 'December'
  ];

  const postElements = blogListElement.querySelectorAll('.post');
  postElements.forEach((postElement) => {
    const titleElement = postElement.querySelector('.post-head-title');
    const subtitleElement = postElement.querySelector('.post-head-subtitle');

    let title = titleElement.innerText;
    let subtitle = subtitleElement.innerText;

    const originalDate = new Date(subtitle);

    // Remove author from title
    let author = title.substring(0, title.indexOf(': '));
    title = title.substring(title.indexOf(': ') + 1).trim();

    // Date format conversion
    const dayOfWeek = daysOfWeek[originalDate.getDay()];
    const month = months[originalDate.getMonth()];
    const day = originalDate.getDate();
    const year = originalDate.getFullYear();
    const formattedDate = `${dayOfWeek}, ${month} ${day}, ${year}`;

    // Update subtitle with formatted date and author
    subtitle = `${formattedDate} \u2014 by ${author}`;

    // Update the text nodes
    titleElement.innerText = title;
    subtitleElement.innerText = subtitle;
  });
}

/**
  * Format the blog posts once the blog list has been populated.
  */
const formatPostsOnLoad = () => { 
  // Constrain the scope of this script to the .blogs-and-videos container class.
  const blogListElement = document.querySelector('.blogs-and-videos .solstice-rss-blog-list');
  if (!blogListElement) {
    return;
  }

  // Observe the blog list for changes. If children are added, we can assume
  // the blog list has been populated or the loading spinner appeared.
  const observeBlogList = (mutationList, observer) => {
    mutationList.forEach((mutation) => {
      if (mutation.type === 'childList') {
        // If the blog list is still loading, do nothing.
        if (blogListElement.querySelector('.solstice-loading')) {
          return;
        }
        // Stop observing the blog list once it's populated. Format the posts.
        observer.disconnect();
        formatPosts(blogListElement);
      }
    });
  };

  const observer = new MutationObserver(observeBlogList);
  observer.observe(blogListElement, { childList: true });
}

formatPostsOnLoad();
